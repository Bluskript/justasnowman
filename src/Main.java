package me.dkorennykh.snowman;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Listener.IListener;
import me.dkorennykh.snowman.Scene.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Danil Korennykh
 *
 * First AP Compsci Program
 *
 * idk why i spent this much time on this lol...
 */
public class Main {
    private static void Moon() {
        StdDraw.setPenColor(new Color(181, 181, 181));
        StdDraw.filledCircle(0, 100, 15);
        StdDraw.setPenColor(new Color(97, 97, 97));
        StdDraw.filledCircle(0, 100, 2);
        StdDraw.filledCircle(5, 95, 2);
    }

    private static List<IListener> EventListeners = new ArrayList<>();

    public static void main(String[] args) {
        StdDraw.enableDoubleBuffering();
        EventListeners.add(new Environment());
        EventListeners.add(new Snowman());
        EventListeners.add(new SnowLayer(10));
        EventListeners.add(new Snow(500));
        EventListeners.add(new Watermark());
        StdDraw.setXscale(0, 100);
        StdDraw.setYscale(0, 100);
        StdDraw.setPenColor(new Color(240, 240, 240));
        StdDraw.filledRectangle(0, 0, 100, 10);
        StdDraw.show();

        while (true) {
            StdDraw.clear();
            for(IListener listener : EventListeners) {
                listener.tick();
            }
            StdDraw.show();
        }
    }
}
