package me.dkorennykh.snowman.Helper;

import java.awt.*;

public class ColorHelper {
    /**
     * Adapted from {@link https://stackoverflow.com/questions/17544157/generate-n-colors-between-two-colors}
     * @param c0 Color 1
     * @param c1 Color 2
     * @param b range from 0 to 1, the blend percent
     * @return a blended color
     */
    public static Color blend(Color c0, Color c1, float b) {
        float iBlend = 1 - b;
        float red = c0.getRed() * b + c1.getRed() * iBlend;
        float green = c0.getGreen() * b + c1.getGreen() * iBlend;
        float blue = c0.getBlue() * b + c1.getBlue() * iBlend;
        return new Color(red / 255, green / 255, blue / 255);
    }
}
