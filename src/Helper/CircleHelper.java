package me.dkorennykh.snowman.Helper;

/**
 * This helps me save circle data
 */
public class CircleHelper {
    private double x;
    private double y;
    private double radius;

    public CircleHelper(double x, double y, double radius) {
        this.x = x;
        this.y = y;
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
