package me.dkorennykh.snowman.Helper;

import java.awt.*;

public class RotationHelper {
    /**
     * Adapted from https://stackoverflow.com/questions/25923480/simple-circle-rotation-simulate-motion
     *
     * TODO : STOP JITTERY BEHAVIOR
     *
     * @param degrees the amount of degrees to get the point at
     * @param r the radius of the virtual circle
     * @return a point on the circle
     */
    public static float[] getPointOnCircle(double degrees, float r, Point center) {
        float x = (float)(r*Math.cos(Math.toRadians(degrees + 90)) + center.getX());
        float y = (float)(r*Math.sin(Math.toRadians(degrees + 90)) + center.getY());
        return new float[]{x, y};
    }
}
