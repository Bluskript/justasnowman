package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Helper.CircleHelper;
import me.dkorennykh.snowman.Listener.IListener;

import java.awt.*;

public class Sun implements IListener {
    private CircleHelper sunManager;

    Sun(float x, float y) {
        sunManager = new CircleHelper(x, y, 8);
    }

    public void setX(float x) {
        sunManager.setX(x);
    }

    public void setY(float y) {
        sunManager.setY(y);
    }

    @Override
    public void tick() {
        StdDraw.setPenColor(new Color(252, 186, 3));
        StdDraw.filledCircle(sunManager.getX(), sunManager.getY(), sunManager.getRadius());
    }
}
