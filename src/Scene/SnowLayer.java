package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Listener.IListener;

public class SnowLayer implements IListener {
    private float height;

    public SnowLayer(int height) {
        this.height = height;
    }


    @Override
    public void tick() {
        // the snow layer will keep growing until it buries the snowman :O
        height += 0.005;
        StdDraw.setPenColor(255, 255, 255);
        StdDraw.filledRectangle(0, 0, 100, height);
    }
}
