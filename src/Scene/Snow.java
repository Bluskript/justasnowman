package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Helper.CircleHelper;
import me.dkorennykh.snowman.Listener.IListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Snow particles make the scene so much cooler...
 */
public class Snow implements IListener {
    private List<CircleHelper> SnowParticles = new ArrayList<>();
    private int windTicks = 0;
    private double[] windDirection = new double[] { 0, 0 };

    public Snow(int particleCount) {
        windDirection[0] = ThreadLocalRandom.current().nextDouble(0, 0.1);
        windDirection[1] = ThreadLocalRandom.current().nextDouble(0, 0.1);
        for (int i = 0; i < particleCount; i++) {
            SnowParticles.add(new CircleHelper(ThreadLocalRandom.current().nextDouble(0, 100), ThreadLocalRandom.current().nextDouble(100, 200), 1));
        }
    }

    @Override
    public void tick() {
        windTicks++;
        // random wind change times
        if(windTicks >= ThreadLocalRandom.current().nextInt(500, 1000)) {
            windTicks = 0;
            windDirection[0] = ThreadLocalRandom.current().nextDouble(-0.02, 0.02);
            windDirection[1] = ThreadLocalRandom.current().nextDouble(-0.02, 0.02);
        }

        for(CircleHelper c: SnowParticles) {
            if(c.getY() < -1 || c.getX() < -1 || c.getX() > 101) {
                c.setY(100);
                c.setX(ThreadLocalRandom.current().nextDouble(0, 100));
            }
            c.setY(c.getY() - 0.1 + ThreadLocalRandom.current().nextDouble(-0.1, 0.1) + windDirection[0]);
            c.setX(c.getX() - ThreadLocalRandom.current().nextDouble(-0.1, 0.1) + windDirection[1]);
            StdDraw.filledCircle(c.getX(), c.getY(), c.getRadius());
        }
    }
}
