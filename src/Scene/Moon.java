package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Helper.CircleHelper;
import me.dkorennykh.snowman.Listener.IListener;

import java.awt.*;

/**
 * Well... It's the moon...
 */
public class Moon implements IListener {
    private CircleHelper moonManager;

    Moon(float x, float y) {
        moonManager = new CircleHelper(x, y, 8);
    }

    public void setX(float x) {
        moonManager.setX(x);
    }

    public void setY(float y) {
        moonManager.setY(y);
    }

    @Override
    public void tick() {
        StdDraw.setPenColor(new Color(181, 181, 181));
        StdDraw.filledCircle(moonManager.getX(), moonManager.getY(), moonManager.getRadius());
    }
}
