package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Listener.IListener;

public class Watermark implements IListener {
    @Override
    public void tick() {
        StdDraw.setPenColor(0, 0, 0);
        StdDraw.text(23, 5, "Made by Danil Korennykh");
        StdDraw.text(73, 5, "I spent too much time on this xd");
    }
}
