package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Helper.ColorHelper;
import me.dkorennykh.snowman.Helper.RotationHelper;
import me.dkorennykh.snowman.Listener.IListener;

import java.awt.*;

public class Environment implements IListener {
    private Moon moon;
    private Sun sun;
    private Color dayColor;
    private Color nightColor;
    private Integer time;

    public Environment() {
        float[] moonPos = RotationHelper.getPointOnCircle(0, 80, new Point(50, 0));
        float[] sunPos = RotationHelper.getPointOnCircle(90, 80, new Point(50, 0));
        moon = new Moon(moonPos[0], moonPos[1]);
        sun = new Sun(sunPos[0], sunPos[1]);
        time = 0;
        dayColor = new Color(144, 209, 222);
        nightColor = new Color(47, 48, 56);
    }

    private void doDayNightCycle() {
        time++;
        int DAY_LENGTH = 10000;
        if(time == DAY_LENGTH) {
            time = 0;
        }

        // day night cycle happens here...
        double moonProgress = (360 * ((double)time / (double)DAY_LENGTH));
        double sunProgress = moonProgress + 180;

        float[] newMoonPos = RotationHelper.getPointOnCircle(moonProgress, 80, new Point(50, 0));
        float[] newSunPos = RotationHelper.getPointOnCircle(sunProgress, 80, new Point(50, 0));
        moon.setX(newMoonPos[0]);
        moon.setY(newMoonPos[1]);
        sun.setX(newSunPos[0]);
        sun.setY(newSunPos[1]);

        Color envColor = ColorHelper.blend(dayColor, nightColor,
                (float)Math.sin(
                        Math.toRadians(
                                (double)time / (double) DAY_LENGTH * 180
                        )
                )
        );

        StdDraw.setPenColor(envColor);
        StdDraw.filledRectangle(0, 0, 100, 100);

        moon.tick();
        sun.tick();
    }

    @Override
    public void tick() {
        doDayNightCycle();
    }
}
