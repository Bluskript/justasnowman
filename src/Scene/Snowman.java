package me.dkorennykh.snowman.Scene;

import edu.princeton.cs.introcs.StdDraw;
import me.dkorennykh.snowman.Helper.CircleHelper;
import me.dkorennykh.snowman.Listener.IListener;

/**
 * Well... It's the snowman...
 */
public class Snowman implements IListener {
    private CircleHelper Base = new CircleHelper(50, 10, 20);
    private CircleHelper Body = new CircleHelper(50, 30, 15);
    private CircleHelper Head = new CircleHelper(50, 50, 12);
    private CircleHelper Eye1 = new CircleHelper(45, 55, 2);
    private CircleHelper Eye2 = new CircleHelper(55, 55, 2);
    private CircleHelper[] Smile = new CircleHelper[] {
            new CircleHelper(44, 47, 1),
            new CircleHelper(46, 45, 1),
            new CircleHelper(49, 45, 1),
            new CircleHelper(52, 45, 1),
            new CircleHelper(55, 47, 1)
    };

    public Snowman() {

    }

    @Override
    public void tick() {
        StdDraw.setPenColor(230, 230, 230);
        StdDraw.filledCircle(Base.getX(), Base.getY(), Base.getRadius());
        StdDraw.filledCircle(Body.getX(), Body.getY(), Body.getRadius());
        StdDraw.filledCircle(Head.getX(), Head.getY(), Head.getRadius());
        StdDraw.setPenColor(0, 0, 0);
        StdDraw.filledCircle(Eye1.getX(), Eye1.getY(), Eye1.getRadius());
        StdDraw.filledCircle(Eye2.getX(), Eye2.getY(), Eye2.getRadius());
        for(CircleHelper smile: Smile) {
            StdDraw.filledCircle(smile.getX(), smile.getY(), smile.getRadius());
        }
        StdDraw.setPenColor(128, 72, 52);
        StdDraw.setPenRadius(0.01);
        StdDraw.line(35, 30, 20, 35);
        StdDraw.line(65, 30, 80, 35);
    }
}
