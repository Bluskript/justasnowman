package me.dkorennykh.snowman.Listener;

/**
 * A simple listener interface, listens for screen tick events
 */
public interface IListener {
    void tick();
}
